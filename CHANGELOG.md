# Changelog

The format of this file is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Calendar Versioning](https://calver.org/) with the format 'YYYY-0M-0D.MINOR'.

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [2023-04-14.0]

### Added
 * Added a warning icon for failed jobs that are allowed to fail (!25 @dirtcrusher)

### Changed
 * Updated dependencies (!24 @dirtcrusher)

## [2022-02-19.0]

### Changed

* Added some miscellaneous visual improvements (!22 @dirtcrusher)
* Only retrieve data from projects that have CI enabled (@dirtcrusher)
* Improved skipped job icon (!23 @dirtcrusher)
* API token field is now a password field (!23 @dirtcrusher)
* Decreased size of namespace in project title (!23 @dirtcrusher)

### Fixed

* Correctly hide scrollbars in Webkit based browsers (!23 @dirtcrusher)

## [2021-12-31.1]

### Fixed

* Fixed size of 'created' icon (!21 @dirtcrusher)

## [2021-12-31.0]

### Added

* Added links to some objects (!19 @dirtcrusher)

### Changed

* Updated dependencies (!11 @dirtcrusher)

### Fixed

* Fixed bug when updating multiple configuration values at once (!17 @dirtcrusher)
* Fixed some errors (!20 @dirtcrusher)

## [2021-12-30.0]

### Added

* Added caching for PNPM store in CI (!12 @dirtcrusher)
* Added (optional) hiding of pipelines of deleted branches and tags (!13 @dirtcrusher)
* Added descriptions to configuration values (!14 @dirtcrusher)
* Added membership configuration value (!15 @dirtcrusher)
* Added automatic update to pipeline view when pipeline is running (!16 @dirtcrusher)
* Added configuration value for the update period of running pipelines (!16 @dirtcrusher)

### Changed

* Improved readability of configuration dialog (!14 @dirtcrusher)

### Fixed

* Fixed errors when invalid configuration was imported (!14 @dirtcrusher)

## [2021-12-27.0]

### Added

* Added license information (!10 @dirtcrusher)

### Changed

* Updated README.md (!9 @dirtcrusher)
* Updated dependencies (!11 @dirtcrusher)

## [2021-12-26.0]

### Added

* Added CI for building and publishing to GitLab Pages (!2 @dirtcrusher)
* Added CHANGELOG.md (!3 @dirtcrusher)
* Added possibility to import / export configuration values (!7 @dirtcrusher)
* Added optional filter for project names (!8 @dirtcrusher)

### Changed

* Made CI jobs interruptible (!4 @dirtcrusher)
* Use CalVer instead of SemVer (!5 @dirtcrusher)
* Removed build dependency from test jobs (!6 @dirtcrusher)

### Fixed

* Fixed infinite loop with an invalid host/token configuration (!7 @dirtcrusher)
