# GitLab CI Carousel

This webpage allows monitoring of the status of GitLab CI. It is optimized for fullscreen use and is designed to be read
from afar, like on a screen in an open-plan office for example.

It can of course also be used on a normal personal machine for monitoring a single project, but for multi-project CI
monitoring I recommend [timoschwarzer/gitlab-monitor](https://github.com/timoschwarzer/gitlab-monitor) instead.

## Usage

The website is hosted with GitLab Pages [here](https://dirtcrusher.gitlab.io/gitlab-ci-carousel/).

You can configure everything by clicking the floating button in the bottom right corner.

## Screenshots

TODO

## Building

The project uses [PNPM](https://pnpm.io/) as a package manager instead of NPM.

```shell
pnpm install
```

For running a local development server:

```shell
pnpm start
```

For building a static version of the website:

```shell
pnpm build
```

## Roadmap

All future plans / ideas are noted down as [issues](https://gitlab.com/dirtcrusher/gitlab-ci-carousel/-/issues).

Feel free to open issues with suggestions / ideas / bug reports !

## Used libraries / tools

- [Typescript](https://www.typescriptlang.org/)
- [React](https://reactjs.org/)
- [Gitbeaker](https://github.com/jdalrymple/gitbeaker#readme)
- [GitLab SVGs](https://gitlab-org.gitlab.io/gitlab-svgs/)

# License

This project is licensed under the GNU General Public License v3.
