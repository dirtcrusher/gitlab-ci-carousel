/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Gitlab as GitLab } from "@gitbeaker/browser";

import Project from "./Project";

class ProjectCollection {
  private readonly api: InstanceType<typeof GitLab>;

  constructor(host: string, token: string) {
    this.api = new GitLab({ host, token });
  }

  public getProjects(
    nameFilter: string = "",
    membership: boolean = false
  ): Promise<Project[]> {
    return this.api.Projects.all({
      archived: false,
      order_by: "last_activity_at",
      search_namespaces: true,
      search: nameFilter,
      membership: membership,
    }).then((projects) => {
      return projects
        .filter((project) => project["jobs_enabled"])
        .map((project) => new Project(this.api, project));
    });
  }
}

export default ProjectCollection;
