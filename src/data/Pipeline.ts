/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Gitlab as GitLab, Types as GitLabTypes } from "@gitbeaker/browser";

export interface JobSchema extends Record<string, unknown> {
  id: number;
  status: string;
  name: string;
  user: GitLabTypes.UserSchema;
  web_url: string;
  allow_failure: boolean;
}

class Pipeline {
  private api: InstanceType<typeof GitLab>;
  private projectId: number;
  private pipelineData: GitLabTypes.PipelineSchema;
  private pipelineExtendedData: GitLabTypes.PipelineExtendedSchema | null;
  private jobs: JobSchema[];

  constructor(
    api: InstanceType<typeof GitLab>,
    projectId: number,
    pipeline: GitLabTypes.PipelineSchema
  ) {
    this.api = api;
    this.projectId = projectId;
    this.pipelineData = pipeline;
    this.pipelineExtendedData = null;
    this.jobs = [];
  }

  public fetchData(): Promise<void> {
    this.jobs = [];
    return Promise.all([
      this.getExtendedPipelineData(),
      this.getJobs(),
      this.getBridgeJobs(),
    ]).then(() => {});
  }

  private getExtendedPipelineData(): Promise<void> {
    return this.api.Pipelines.show(this.projectId, this.pipelineData.id)
      .then((pipeline) => {
        // An error in the @gitbeaker library, the show() function should
        // return a PipelineExtendedSchema and not a PipelineSchema
        // @ts-ignore
        this.pipelineExtendedData = pipeline;
      })
      .catch(() => this.getExtendedPipelineData());
  }

  private getJobs(): Promise<void> {
    return this.api.Jobs.showPipelineJobs(this.projectId, this.pipelineData.id)
      .then((jobs) => {
        this.jobs = [...this.jobs, ...jobs];
      })
      .catch(() => this.getJobs());
  }

  private getBridgeJobs(): Promise<void> {
    return this.api.Jobs.showPipelineBridges(
      this.projectId,
      this.pipelineData.id,
      {}
    )
      .then((bridges) => {
        // Error in @gitbeaker, bridges is an array and not a single bridge
        // @ts-ignore
        const tmp: GitLabTypes.BridgeSchema[] = bridges;
        this.jobs = [...this.jobs, ...tmp];
      })
      .catch(() => this.getBridgeJobs());
  }

  public getRef(): string {
    return this.pipelineData.ref || "";
  }

  public getId(): number {
    return this.pipelineData.id;
  }

  public getUser() {
    return this.pipelineExtendedData?.user;
  }

  private estimateDuration(): number {
    if (!this.pipelineExtendedData?.started_at) {
      return 0;
    }

    const startDate = new Date(this.pipelineExtendedData.started_at);
    const currentDate = new Date();

    return Math.floor((currentDate.getTime() - startDate.getTime()) / 1000);
  }

  public getDuration(): number {
    if (!this.pipelineExtendedData) {
      return 0;
    }

    if (!this.pipelineExtendedData.duration) {
      return this.estimateDuration();
    } else {
      return Number.parseInt(this.pipelineExtendedData.duration);
    }
  }

  public getStatus(): GitLabTypes.PipelineStatus {
    return this.pipelineExtendedData?.status || this.pipelineData.status;
  }

  public getJobsByStatus(status: GitLabTypes.PipelineStatus): JobSchema[] {
    return this.jobs.filter((job) => job.status === status);
  }

  public hasYamlError(): boolean {
    if (this.pipelineExtendedData === null) {
      return false;
    } else {
      return this.pipelineExtendedData.yaml_errors !== null;
    }
  }

  public getWebUrl(): string {
    return this.pipelineData.web_url;
  }
}

export default Pipeline;
