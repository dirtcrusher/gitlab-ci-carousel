/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Gitlab as GitLab, Types as GitLabTypes } from "@gitbeaker/browser";

import Pipeline from "./Pipeline";

export type PipelinesByRefSchema = Map<string, Pipeline[]>;

class Project {
  private readonly api: InstanceType<typeof GitLab>;
  private projectData: GitLabTypes.ProjectSchema;

  constructor(
    api: InstanceType<typeof GitLab>,
    projectData: GitLabTypes.ProjectSchema
  ) {
    this.api = api;
    this.projectData = projectData;
  }

  public getDefaultPipeline(): Promise<Pipeline | null> {
    return this.api.Pipelines.all(this.projectData.id, {
      perPage: 1,
      maxPages: 1,
      ref: this.projectData.default_branch,
    })
      .then((pipelines) => {
        return pipelines.at(0) || null;
      })
      .then((pipeline) => {
        return pipeline
          ? new Pipeline(this.api, this.projectData.id, pipeline)
          : null;
      })
      .catch(() => this.getDefaultPipeline());
  }

  public getPipelines(maxAge: number): Promise<Pipeline[]> {
    const updated_after_date = new Date(
      new Date().getTime() - maxAge * 60 * 60 * 1000
    );

    return this.api.Pipelines.all(this.projectData.id, {
      maxPages: 1,
      perPage: 100,
      updated_after: updated_after_date.toISOString(),
    })
      .then((pipelines) => {
        return pipelines.map(
          (pipeline) => new Pipeline(this.api, this.projectData.id, pipeline)
        );
      })
      .catch(() => this.getPipelines(maxAge));
  }

  public getBranches(): Promise<GitLabTypes.BranchSchema[]> {
    return this.api.Branches.all(this.projectData.id, {
      perPage: 100,
    });
  }

  public getTags(): Promise<GitLabTypes.TagSchema[]> {
    return this.api.Tags.all(this.projectData.id, { perPage: 100 });
  }

  public static parsePipelineBranches(
    pipelines: Pipeline[]
  ): PipelinesByRefSchema {
    const pipelinesByBranch = new Map<string, Pipeline[]>();
    for (const pipeline of pipelines) {
      const tmp = pipelinesByBranch.get(pipeline.getRef()) || [];
      pipelinesByBranch.set(pipeline.getRef(), [...tmp, pipeline]);
    }

    return pipelinesByBranch;
  }

  public static filterDeletedRefs(
    pipelinesByRef: PipelinesByRefSchema,
    branches: GitLabTypes.BranchSchema[],
    tags: GitLabTypes.TagSchema[]
  ) {
    for (const ref of pipelinesByRef.keys()) {
      if (
        !branches.map((branch) => branch.name).includes(ref) &&
        !tags.map((tag) => tag.name).includes(ref)
      ) {
        pipelinesByRef.delete(ref);
      }
    }
  }

  public static limitHistoryPerRef(
    pipelinesByRef: PipelinesByRefSchema,
    historyPerRef: number
  ) {
    for (const ref of pipelinesByRef.keys()) {
      const pipelines: Pipeline[] = pipelinesByRef.get(ref) || [];
      pipelinesByRef.set(ref, pipelines.slice(0, historyPerRef));
    }
  }

  public getID(): number {
    return this.projectData.id;
  }

  public getNamespace(): string {
    return this.projectData.namespace.full_path;
  }

  public getName(): string {
    return this.projectData.name;
  }
}

export default Project;
