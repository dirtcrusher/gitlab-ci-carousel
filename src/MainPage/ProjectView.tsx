/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useCallback, useContext, useEffect, useState } from "react";

import { Box, Grid, Stack, Typography, useTheme } from "@mui/material";

import { Context as ConfigContext } from "../providers/Config";

import Project, { PipelinesByRefSchema } from "../data/Project";
import Pipeline from "../data/Pipeline";

import { getColor } from "./StatusColors";
import BranchView from "./BranchView";

const ProjectView: React.FC<{ project: Project }> = ({ project }) => {
  const theme = useTheme();
  const { maxAge, historyPerRef, hideDeletedRefs, updatePeriodRunning } =
    useContext(ConfigContext);

  const [pipelinesByBranch, setPipelinesByBranch] =
    useState<PipelinesByRefSchema>(new Map());

  const [defaultPipeline, setDefaultPipeline] = useState<Pipeline | null>(null);
  const [statusColor, setStatusColor] = useState(getColor("success"));

  const getDefaultPipeline = useCallback(() => {
    project.getDefaultPipeline().then((fetchedPipeline) => {
      setDefaultPipeline(fetchedPipeline);
      setStatusColor(getColor(fetchedPipeline?.getStatus() || "success"));
    });
  }, [project]);

  useEffect(() => {
    getDefaultPipeline();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (defaultPipeline?.getStatus() !== "running") {
      return () => {};
    }

    const intervalId = setInterval(
      getDefaultPipeline,
      updatePeriodRunning * 1000
    );

    return function cleanup() {
      clearInterval(intervalId);
    };
  }, [getDefaultPipeline, defaultPipeline, updatePeriodRunning]);

  useEffect(() => {
    Promise.all([
      project.getPipelines(maxAge),
      project.getBranches(),
      project.getTags(),
    ]).then(([fetchedPipelines, fetchedBranches, fetchedTags]) => {
      const fetchedPipelinesByBranch =
        Project.parsePipelineBranches(fetchedPipelines);

      Project.limitHistoryPerRef(fetchedPipelinesByBranch, historyPerRef);

      if (hideDeletedRefs) {
        Project.filterDeletedRefs(
          fetchedPipelinesByBranch,
          fetchedBranches,
          fetchedTags
        );
      }

      setPipelinesByBranch(fetchedPipelinesByBranch);
    });
  }, [maxAge, historyPerRef, hideDeletedRefs, project]);

  if (pipelinesByBranch.size === 0) {
    return <></>;
  }

  return (
    <Grid
      item
      style={{
        minWidth: "100vw",
        scrollSnapAlign: "start",
      }}
    >
      <Box>
        <Stack
          direction={"row"}
          alignItems={"center"}
          justifyContent={"center"}
          spacing={2}
          style={{
            backgroundColor: statusColor,
            color: theme.palette.getContrastText(statusColor),
          }}
        >
          <Typography variant={"h5"}>{project.getNamespace()}/</Typography>
          <Typography variant={"h1"}>{project.getName()}</Typography>
        </Stack>
        <Box sx={{ margin: "1em" }} />
        <Box
          sx={{
            display: "grid",
            rowGap: "1em",
            columnGap: "1em",
            gridTemplateColumns: "repeat(auto-fit, minmax(45vw, 1fr))",
          }}
        >
          {Array.from(pipelinesByBranch).map(([ref, pipelines]) => {
            return (
              <BranchView key={ref} branchName={ref} pipelines={pipelines} />
            );
          })}
        </Box>
      </Box>
    </Grid>
  );
};

export default ProjectView;
