/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useCallback, useContext, useEffect, useState } from "react";

import { Types as GitLabTypes } from "@gitbeaker/browser";

import { Box, Typography, useTheme } from "@mui/material";

import { Context as ConfigContext } from "../providers/Config";

import Pipeline from "../data/Pipeline";

import { getColor } from "./StatusColors";
import PipelineView from "./PipelineView";

const BranchView: React.FC<{
  branchName: string;
  pipelines: Pipeline[];
}> = ({ branchName, pipelines }) => {
  const theme = useTheme();
  const { updatePeriodRunning } = useContext(ConfigContext);

  const [statusColor, setStatusColor] = useState(getColor("success"));

  const getBranchStatus = useCallback((): GitLabTypes.PipelineStatus => {
    return pipelines.at(0)?.getStatus() || "success";
  }, [pipelines]);

  useEffect(() => {
    if (getBranchStatus() !== "running") {
      setStatusColor(getColor(getBranchStatus()));
      return () => {};
    }

    const intervalId = setInterval(() => {
      setStatusColor(getColor(getBranchStatus()));
    }, updatePeriodRunning * 1000);

    return function cleanup() {
      clearInterval(intervalId);
    };
  }, [getBranchStatus, updatePeriodRunning]);

  return (
    <Box>
      <Typography
        variant={"h4"}
        align={"center"}
        style={{
          marginBottom: "5px",
          backgroundColor: statusColor,
          color: theme.palette.getContrastText(statusColor),
        }}
      >
        {branchName}
      </Typography>
      <Box
        sx={{
          display: "grid",
          rowGap: "0.5em",
          gridTemplateColumns: "auto-fit",
        }}
      >
        {pipelines.map((pipeline) => {
          return <PipelineView key={pipeline.getId()} pipeline={pipeline} />;
        })}
      </Box>
    </Box>
  );
};

export default BranchView;
