/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";

import { Grid } from "@mui/material";

import { Context as ConfigContext } from "../providers/Config";

import ProjectCollection from "../data/ProjectCollection";
import Project from "../data/Project";

import ProjectView from "./ProjectView";

import "./ProjectCollectionView.css";

const ProjectCollectionView: React.FC<{
  projectCollection: ProjectCollection;
}> = ({ projectCollection }) => {
  const { swipePeriod, nameFilter, membership } = useContext(ConfigContext);

  const [projects, setProjects] = useState<Project[]>([]);

  const refContainer = useRef<HTMLDivElement>(null);
  const [currentProjectIndex, setCurrentProjectIndex] = useState<number>(0);

  const fetchProjects = useCallback(() => {
    projectCollection
      .getProjects(nameFilter, membership)
      .then((fetchedProjects) => {
        setProjects(fetchedProjects);
      });
  }, [projectCollection, nameFilter, membership]);

  const resetCarousel = useCallback(() => {
    setProjects([]);
    fetchProjects();
  }, [fetchProjects]);

  const swipeToNextProject = useCallback(() => {
    setCurrentProjectIndex((prevIndex) => {
      if (!refContainer.current) {
        return prevIndex;
      }

      const maxIndex =
        Math.ceil(refContainer.current.scrollWidth / window.innerWidth) - 1;
      if (prevIndex + 1 > maxIndex) {
        fetchProjects();
        return 0;
      } else {
        return prevIndex + 1;
      }
    });
  }, [fetchProjects]);

  useEffect(() => {
    resetCarousel();
  }, [resetCarousel]);

  useEffect(() => {
    const swipeInterval = setInterval(swipeToNextProject, swipePeriod * 1000);

    return function cleanup() {
      clearInterval(swipeInterval);
    };
  }, [swipePeriod, swipeToNextProject]);

  useEffect(() => {
    if (!refContainer.current) {
      return;
    }

    console.log("Swiping to index '" + currentProjectIndex + "'");
    refContainer.current.scrollTo({
      left: currentProjectIndex * window.innerWidth,
      top: 0,
      behavior: "smooth",
    });
  }, [currentProjectIndex, refContainer]);

  return (
    <div
      ref={refContainer}
      style={{
        overflow: "scroll",
        scrollSnapType: "x mandatory",
        scrollbarWidth: "none",
        msOverflowStyle: "none",
        minHeight: "100vh",
      }}
      dir={"ltr"}
    >
      <Grid container direction={"row"} wrap={"nowrap"}>
        {projects.map((project) => {
          return <ProjectView key={project.getID()} project={project} />;
        })}
      </Grid>
    </div>
  );
};

export default ProjectCollectionView;
