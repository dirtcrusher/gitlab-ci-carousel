/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Types as GitLabTypes } from "@gitbeaker/browser";

export const getColor = (
  status: GitLabTypes.PipelineStatus,
  allow_failure: boolean = false
): string => {
  switch (status) {
    case "failed":
      if (allow_failure) {
        return "#ab6100";
      } else {
        return "#ec5940";
      }

    case "success":
      return "#108548";

    case "canceled":
    case "skipped":
    case "scheduled":
    case "manual":
      return "#fafafa";

    case "preparing":
      return "#999999";

    case "pending":
    case "waiting_for_resource":
      return "#ab6100";

    case "running":
      return "#1f75cb";

    case "created":
      return "#868686";
  }
};
