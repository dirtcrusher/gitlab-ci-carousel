/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from "react";

import { Chip, Grid, Link, Paper, Typography, useTheme } from "@mui/material";

import { Context as ConfigContext } from "../providers/Config";

import Pipeline, { JobSchema } from "../data/Pipeline";

import { TimerIcon, UserIcon } from "../icons";

import formatDuration from "../utils/DurationFormatter";

import { getColor } from "./StatusColors";
import JobsView from "./JobsView";

const PipelineView: React.FC<{ pipeline: Pipeline }> = ({ pipeline }) => {
  const theme = useTheme();
  const { updatePeriodRunning } = useContext(ConfigContext);

  const [successJobs, setSuccessJobs] = useState<JobSchema[]>([]);
  const [failedJobs, setFailedJobs] = useState<JobSchema[]>([]);
  const [runningJobs, setRunningJobs] = useState<JobSchema[]>([]);
  const [createdJobs, setCreatedJobs] = useState<JobSchema[]>([]);
  const [pendingJobs, setPendingJobs] = useState<JobSchema[]>([]);
  const [skippedJobs, setSkippedJobs] = useState<JobSchema[]>([]);
  const [canceledJobs, setCanceledJobs] = useState<JobSchema[]>([]);

  const categorizeJobs = (sourcePipeline: Pipeline) => {
    setSuccessJobs(sourcePipeline.getJobsByStatus("success"));
    setFailedJobs(sourcePipeline.getJobsByStatus("failed"));
    setRunningJobs(sourcePipeline.getJobsByStatus("running"));
    setCreatedJobs(sourcePipeline.getJobsByStatus("created"));
    setPendingJobs([
      ...sourcePipeline.getJobsByStatus("waiting_for_resource"),
      ...sourcePipeline.getJobsByStatus("preparing"),
      ...sourcePipeline.getJobsByStatus("pending"),
      ...sourcePipeline.getJobsByStatus("scheduled"),
      ...sourcePipeline.getJobsByStatus("manual"),
    ]);
    setSkippedJobs(sourcePipeline.getJobsByStatus("skipped"));
    setCanceledJobs(sourcePipeline.getJobsByStatus("canceled"));
  };

  useEffect(() => {
    if (pipeline.getStatus() !== "running") {
      pipeline.fetchData().then(() => {
        categorizeJobs(pipeline);
      });
      return () => {};
    }

    const intervalId = setInterval(() => {
      pipeline.fetchData().then(() => {
        categorizeJobs(pipeline);
      });
    }, updatePeriodRunning * 1000);

    return function cleanup() {
      clearInterval(intervalId);
    };
  }, [pipeline, updatePeriodRunning]);

  return (
    <Paper>
      <Grid
        container
        direction={"row"}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Grid item>
          <Link
            href={pipeline.getWebUrl()}
            target={"_blank"}
            rel={"noopener"}
            underline={"none"}
            color={"inherit"}
          >
            <Typography
              component={"span"}
              sx={{
                fontFamily: "monospace",
                marginLeft: "1em",
                marginRight: "0.5em",
              }}
            >
              #{pipeline.getId()}
            </Typography>
          </Link>
        </Grid>

        <Grid item>
          {pipeline.hasYamlError() && (
            <Chip
              variant={"outlined"}
              sx={{
                fontFamily: "monospace",
                color: theme.palette.getContrastText(getColor("failed")),
                backgroundColor: getColor("failed"),
                marginLeft: "0.5em",
                marginRight: "0.5em",
              }}
              label={"YAML error"}
            />
          )}

          <JobsView status={"success"} jobs={successJobs} collapsed={true} />

          <JobsView status={"failed"} jobs={failedJobs} collapsed={false} />

          <JobsView status={"running"} jobs={runningJobs} collapsed={false} />

          <JobsView status={"created"} jobs={createdJobs} collapsed={false} />

          <JobsView status={"pending"} jobs={pendingJobs} collapsed={false} />

          <JobsView status={"skipped"} jobs={skippedJobs} collapsed={true} />

          <JobsView status={"canceled"} jobs={canceledJobs} collapsed={true} />
        </Grid>

        <Grid item xs />

        <Grid item>
          <Chip
            variant={"outlined"}
            sx={{
              fontFamily: "monospace",
              marginLeft: "0.5em",
              marginRight: "0.5em",
            }}
            icon={<TimerIcon />}
            label={formatDuration(pipeline.getDuration())}
          />

          <Link
            href={pipeline.getUser()?.web_url || undefined}
            target={"_blank"}
            rel={"noopener"}
            underline={"none"}
            color={"inherit"}
            sx={{
              marginLeft: "0.5em",
              marginRight: "0.5em",
            }}
          >
            <Chip
              variant={"outlined"}
              icon={<UserIcon />}
              label={pipeline.getUser()?.name || ""}
              sx={{ cursor: "inherit" }}
            />
          </Link>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default PipelineView;
