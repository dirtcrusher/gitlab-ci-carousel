/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from "react";

import { Context as ConfigContext } from "../providers/Config";

import ProjectCollection from "../data/ProjectCollection";

import ConfigController from "../components/ConfigController";

import ProjectCollectionView from "./ProjectCollectionView";

const MainPage: React.FC = () => {
  const { host, token } = useContext(ConfigContext);

  const [projectCollection, setProjectCollection] = useState(
    new ProjectCollection(host, token)
  );

  useEffect(() => {
    setProjectCollection(new ProjectCollection(host, token));
  }, [host, token]);

  return (
    <>
      <ConfigController />
      <ProjectCollectionView projectCollection={projectCollection} />
    </>
  );
};

export default MainPage;
