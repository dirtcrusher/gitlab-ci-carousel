/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { Types as GitLabTypes } from "@gitbeaker/browser";

import { Chip, Link, Typography, useTheme } from "@mui/material";

import { JobSchema } from "../data/Pipeline";

import { getColor } from "./StatusColors";

import {
  StatusCanceledIcon,
  StatusCreatedIcon,
  StatusFailedIcon,
  StatusPendingIcon,
  StatusRunningIcon,
  StatusSkippedIcon,
  StatusSuccessIcon,
  StatusWarningIcon,
} from "../icons";

const getIcon = (
  status: GitLabTypes.PipelineStatus,
  allow_failure: boolean = false
) => {
  switch (status) {
    case "canceled":
      return <StatusCanceledIcon />;

    case "created":
      return <StatusCreatedIcon />;

    case "failed":
      if (allow_failure) {
        return <StatusWarningIcon />;
      } else {
        return <StatusFailedIcon />;
      }

    case "pending":
      return <StatusPendingIcon />;

    case "running":
      return <StatusRunningIcon />;

    case "skipped":
      return <StatusSkippedIcon />;

    case "success":
      return <StatusSuccessIcon />;

    default:
      return <StatusCreatedIcon />;
  }
};

const JobsView: React.FC<{
  status: GitLabTypes.PipelineStatus;
  jobs: JobSchema[];
  collapsed: boolean;
}> = ({ status, jobs, collapsed }) => {
  const theme = useTheme();

  if (jobs.length === 0) {
    return <></>;
  }

  if (collapsed) {
    return (
      <Chip
        variant={"outlined"}
        sx={{
          color: theme.palette.getContrastText(getColor(status)),
          backgroundColor: getColor(status),
          marginLeft: "0.5em",
          marginRight: "0.5em",
        }}
        icon={getIcon(status)}
        label={
          <Typography
            align={"center"}
            sx={{ fontFamily: "monospace", width: "2ch" }}
          >
            {jobs.length}
          </Typography>
        }
      />
    );
  } else {
    return (
      <>
        {jobs.map((job) => (
          <Link
            key={job.id}
            href={job.web_url}
            target={"_blank"}
            rel={"noopener"}
            underline={"none"}
            color={"inherit"}
            sx={{
              marginLeft: "0.5em",
              marginRight: "0.5em",
            }}
          >
            <Chip
              variant={"outlined"}
              sx={{
                backgroundColor: getColor(status, job.allow_failure),
                color: theme.palette.getContrastText(
                  getColor(status, job.allow_failure)
                ),
                cursor: "inherit",
              }}
              icon={getIcon(status, job.allow_failure)}
              label={
                <Typography align={"center"} sx={{ fontFamily: "monospace" }}>
                  {job.name}
                </Typography>
              }
            />
          </Link>
        ))}
      </>
    );
  }
};

export default JobsView;
