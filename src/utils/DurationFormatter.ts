/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const parseDuration = (duration: number): string => {
  let remainingSeconds = duration;
  const numHours = Math.floor(remainingSeconds / (60 * 60));
  remainingSeconds = remainingSeconds - numHours * 60 * 60;

  const numMinutes = Math.floor(remainingSeconds / 60);
  remainingSeconds = remainingSeconds - numMinutes * 60;

  if (numHours > 0) {
    return (
      numHours.toString() +
      ":" +
      numMinutes.toString().padStart(2, "0") +
      ":" +
      remainingSeconds.toString().padStart(2, "0")
    );
  } else if (numMinutes > 0) {
    return (
      numMinutes.toString() + ":" + remainingSeconds.toString().padStart(2, "0")
    );
  } else {
    return "0:" + remainingSeconds.toString().padStart(2, "0");
  }
};

export default parseDuration;
