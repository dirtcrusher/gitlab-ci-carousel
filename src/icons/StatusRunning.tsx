/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { SvgIcon, SvgIconProps } from "@mui/material";

// The following svg code is copied from @gitlab/svgs
// https://gitlab.com/gitlab-org/gitlab-svgs/blob/v2.0.0/sprite_icons/status-running.svg
const raw = (
  <svg width="12" height="12" xmlns="http://www.w3.org/2000/svg">
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M6 12A6 6 0 106 0a6 6 0 000 12zm-.629-8.42a2.5 2.5 0 011.45.059.75.75 0 00.493-1.417 4 4 0 102.464 5.092.75.75 0 10-1.417-.493 2.5 2.5 0 11-2.99-3.24z"
      fill="#000"
    />
  </svg>
);

const StatusRunning: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon {...props} viewBox={"0 0 12 12"}>
      {raw}
    </SvgIcon>
  );
};

export default StatusRunning;
