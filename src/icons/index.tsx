/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import StatusCanceled from "./StatusCanceled";
import StatusCreated from "./StatusCreated";
import StatusFailed from "./StatusFailed";
import StatusPending from "./StatusPending";
import StatusRunning from "./StatusRunning";
import StatusSkipped from "./StatusSkipped";
import StatusSuccess from "./StatusSuccess";
import StatusWarning from "./StatusWarning";
import Timer from "./Timer";
import User from "./User";

export {
  StatusCanceled as StatusCanceledIcon,
  StatusCreated as StatusCreatedIcon,
  StatusFailed as StatusFailedIcon,
  StatusPending as StatusPendingIcon,
  StatusRunning as StatusRunningIcon,
  StatusSkipped as StatusSkippedIcon,
  StatusSuccess as StatusSuccessIcon,
  StatusWarning as StatusWarningIcon,
  Timer as TimerIcon,
  User as UserIcon,
};
