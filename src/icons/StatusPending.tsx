/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { SvgIcon, SvgIconProps } from "@mui/material";

// The following svg code is copied from @gitlab/svgs
// https://gitlab.com/gitlab-org/gitlab-svgs/blob/v2.0.0/sprite_icons/status_pending.svg
const raw = (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g fillRule="evenodd">
      <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z" />
      <path
        d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
        fill="#FFF"
        style={{ fill: "var(--svg-status-bg, #fff)" }}
      />
      <path d="M4.7 5.3c0-.2.1-.3.3-.3h.9c.2 0 .3.1.3.3v3.4c0 .2-.1.3-.3.3H5c-.2 0-.3-.1-.3-.3V5.3m3 0c0-.2.1-.3.3-.3h.9c.2 0 .3.1.3.3v3.4c0 .2-.1.3-.3.3H8c-.2 0-.3-.1-.3-.3V5.3" />
    </g>
  </svg>
);

const StatusPending: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon {...props} viewBox={"0 0 14 14"}>
      {raw}
    </SvgIcon>
  );
};

export default StatusPending;
