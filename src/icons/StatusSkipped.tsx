/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { SvgIcon, SvgIconProps } from "@mui/material";

// The following svg code is copied from @gitlab/svgs
// https://gitlab.com/gitlab-org/gitlab-svgs/blob/v2.0.0/sprite_icons/status_skipped.svg
// The only modification is the background color from #fff to #000
const raw = (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M7 14A7 7 0 1 1 7 0a7 7 0 0 1 0 14z" />
    <path
      d="M7 13A6 6 0 1 0 7 1a6 6 0 0 0 0 12z"
      fill="#FFF"
      fillRule="nonzero"
      style={{ fill: "var(--svg-status-bg, #000)" }}
    />
    <path d="M6.415 7.04L4.579 5.203a.295.295 0 0 1 .004-.416l.349-.349a.29.29 0 0 1 .416-.004l2.214 2.214a.289.289 0 0 1 .019.021l.132.133c.11.11.108.291 0 .398L5.341 9.573a.282.282 0 0 1-.398 0l-.331-.331a.285.285 0 0 1 0-.399L6.415 7.04zm2.54 0L7.119 5.203a.295.295 0 0 1 .004-.416l.349-.349a.29.29 0 0 1 .416-.004l2.214 2.214a.289.289 0 0 1 .019.021l.132.133c.11.11.108.291 0 .398L7.881 9.573a.282.282 0 0 1-.398 0l-.331-.331a.285.285 0 0 1 0-.399L8.955 7.04z" />
  </svg>
);

const StatusSkipped: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon {...props} viewBox={"0 0 14 14"}>
      {raw}
    </SvgIcon>
  );
};

export default StatusSkipped;
