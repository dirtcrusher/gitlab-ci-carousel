/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { SvgIcon, SvgIconProps } from "@mui/material";

// The following svg code is copied from @gitlab/svgs
// https://gitlab.com/gitlab-org/gitlab-svgs/-/blob/v2.0.0/sprite_icons/status-success.svg
const raw = (
  <svg width="12" height="12" xmlns="http://www.w3.org/2000/svg">
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M6 12A6 6 0 106 0a6 6 0 000 12zm2.576-7.02a.75.75 0 00-1.152-.96L5.45 6.389l-.92-.92A.75.75 0 003.47 6.53l1.5 1.5a.75.75 0 001.106-.05l2.5-3z"
      fill="#000"
    />
  </svg>
);

const StatusSuccess: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon {...props} viewBox={"0 0 12 12"}>
      {raw}
    </SvgIcon>
  );
};

export default StatusSuccess;
