/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import React, { ReactElement } from "react";

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from "@mui/material";

interface FieldData {
  name: string;
  input: ReactElement;
  description: string | ReactElement;
}

const ConfigurationColumn: React.FC<{ fields: FieldData[] }> = ({ fields }) => {
  return (
    <TableContainer>
      <Table>
        <TableBody>
          {fields.map((field) => (
            <TableRow key={field.name} hover>
              <TableCell align={"right"}>
                <Typography>{field.name}</Typography>
              </TableCell>
              <TableCell align={"center"}>{field.input}</TableCell>
              <TableCell align={"center"}>
                {typeof field.description === "string" ? (
                  <Typography>{field.description}</Typography>
                ) : (
                  field.description
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ConfigurationColumn;
