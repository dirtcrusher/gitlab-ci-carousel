/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from "react";

import { IconButton, InputAdornment, TextField } from "@mui/material";

import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@mui/icons-material";

import { Context as ConfigContext } from "../../providers/Config";

const FieldAPIToken: React.FC = () => {
  const { token, updateConfigEntry } = useContext(ConfigContext);

  const [value, setValue] = useState<string>(token);
  const [showRaw, setShowRaw] = useState<boolean>(false);

  useEffect(() => {
    setValue(token);
  }, [token]);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(event.target.value);
    updateConfigEntry("token", event.target.value);
  };

  return (
    <TextField
      value={value}
      onChange={onChange}
      type={showRaw ? "text" : "password"}
      fullWidth
      InputProps={{
        endAdornment: (
          <InputAdornment position={"end"}>
            <IconButton onClick={() => setShowRaw(!showRaw)} edge={"end"}>
              {showRaw ? <VisibilityOffIcon /> : <VisibilityIcon />}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  );
};

export default FieldAPIToken;
