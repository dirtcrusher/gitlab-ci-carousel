/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from "react";

import { TextField } from "@mui/material";

import { Context as ConfigContext } from "../../providers/Config";

const FieldNameFilter: React.FC = () => {
  const { nameFilter, updateConfigEntry } = useContext(ConfigContext);

  const [value, setValue] = useState<string>(nameFilter);

  useEffect(() => {
    setValue(nameFilter);
  }, [nameFilter]);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(event.target.value);
    updateConfigEntry("nameFilter", event.target.value);
  };

  return (
    <TextField
      value={value}
      placeholder={"No filter"}
      onChange={onChange}
      fullWidth
    />
  );
};

export default FieldNameFilter;
