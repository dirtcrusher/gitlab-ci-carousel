/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from "react";

import { TextField } from "@mui/material";

import { Context as ConfigContext } from "../../providers/Config";

const FieldMaxAge: React.FC = () => {
  const { maxAge, updateConfigEntry } = useContext(ConfigContext);

  const [value, setValue] = useState<string>(maxAge.toString());

  useEffect(() => {
    setValue(maxAge.toString());
  }, [maxAge]);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(event.target.value);
    const parsedNumber = Number.parseInt(event.target.value);
    if (!isNaN(parsedNumber)) {
      updateConfigEntry("maxAge", parsedNumber);
    }
  };

  return <TextField value={value} onChange={onChange} fullWidth />;
};

export default FieldMaxAge;
