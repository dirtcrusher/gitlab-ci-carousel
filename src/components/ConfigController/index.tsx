/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useContext, useState } from "react";

import {
  Button,
  Collapse,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Fab,
  Grid,
  Typography,
  useTheme,
} from "@mui/material";

import { Settings as SettingsIcon } from "@mui/icons-material";

import { Context as ConfigContext } from "../../providers/Config";

import FieldAPIHost from "./FieldAPIHost";
import ConfigurationColumn from "./ConfigurationColumn";
import FieldAPIToken from "./FieldAPIToken";
import FieldMaxAge from "./FieldMaxAge";
import FieldSwipePeriod from "./FieldSwipePeriod";
import FieldHistoryPerRef from "./FieldHistoryPerRef";
import FieldHideDeletedRefs from "./FieldHideDeletedRefs";
import FieldNameFilter from "./FieldNameFilter";
import FieldMembership from "./FieldMembership";
import FieldUpdatePeriodRunning from "./FieldUpdatePeriodRunning";

const ConfigController: React.FC = () => {
  const theme = useTheme();

  const {
    saveModifiedConfiguration,
    cancelConfigurationModification,
    getCompleteConfig,
    setCompleteConfig,
  } = useContext(ConfigContext);

  const [isHovering, setIsHovering] = useState<boolean>(false);
  const [dialogOpened, setDialogOpened] = useState<boolean>(false);

  const onFabMouseEnter: VoidFunction = () => {
    setIsHovering(true);
  };

  const onFabMouseLeave: VoidFunction = () => {
    setIsHovering(false);
  };

  const onFabClick: VoidFunction = () => {
    setDialogOpened(true);
  };

  const onDialogCancel: VoidFunction = () => {
    cancelConfigurationModification();
    setDialogOpened(false);
  };

  const onDialogSave: VoidFunction = () => {
    saveModifiedConfiguration();
    setDialogOpened(false);
  };

  const onDialogImport: VoidFunction = () => {
    const input = document.createElement("input");
    input.type = "file";
    input.onchange = (fileChangeEvent) => {
      const reader = new FileReader();
      reader.onloadend = (loadEvent) => {
        if (loadEvent.target?.result) {
          setCompleteConfig(JSON.parse(loadEvent.target?.result.toString()));
        }
      };
      // @ts-ignore
      reader.readAsText(fileChangeEvent.target?.files[0]);
    };
    document.body.appendChild(input);
    input.click();
    document.body.removeChild(input);
  };

  const onDialogExport: VoidFunction = () => {
    const link = document.createElement("a");
    const contents = new Blob([JSON.stringify(getCompleteConfig())], {
      type: "text/json",
    });
    link.href = URL.createObjectURL(contents);
    link.download = "carousel-config.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <>
      <Fab
        onMouseEnter={onFabMouseEnter}
        onMouseLeave={onFabMouseLeave}
        onClick={onFabClick}
        color={"primary"}
        size={"small"}
        variant={"extended"}
        sx={{
          position: "fixed",
          right: theme.spacing(1),
          bottom: theme.spacing(1),
        }}
      >
        <SettingsIcon />
        <Collapse in={isHovering} orientation={"horizontal"}>
          Configuration
        </Collapse>
      </Fab>
      <Dialog open={dialogOpened} fullScreen>
        <DialogTitle>
          <Typography align={"center"} variant={"inherit"}>
            Configuration
          </Typography>
        </DialogTitle>
        <DialogContent>
          <ConfigurationColumn
            fields={[
              {
                name: "Instance URL",
                input: <FieldAPIHost />,
                description: (
                  <Typography>
                    The URL of the GitLab instance. Note that it shouldn't
                    include the{" "}
                    <span style={{ fontFamily: "monospace" }}>/api/v4</span>.
                  </Typography>
                ),
              },
              {
                name: "Access Token",
                input: <FieldAPIToken />,
                description: (
                  <Typography>
                    The access token used to access the API. It requires the{" "}
                    <span style={{ fontFamily: "monospace" }}>TODO</span>{" "}
                    permissions.
                  </Typography>
                ),
              },
              {
                name: "Max pipeline age",
                input: <FieldMaxAge />,
                description:
                  "The maximum age (in hours) of the pipelines to show.",
              },
              {
                name: "Project switch period",
                input: <FieldSwipePeriod />,
                description:
                  "The duration for which a project is shown before switching to the next one.",
              },
              {
                name: "History per branch / tag",
                input: <FieldHistoryPerRef />,
                description:
                  "The maximum number of pipelines to show for a branch or a tag.",
              },
              {
                name: "Hide deleted branches / tags",
                input: <FieldHideDeletedRefs />,
                description:
                  "Hide branches or tags that have recent pipelines but don't exist anymore.",
              },
              {
                name: "Filter for project names",
                input: <FieldNameFilter />,
                description: (
                  <>
                    <Typography>
                      Only show projects that contain this filter in their full
                      name (including namespace).
                    </Typography>
                    <Typography fontStyle={"italic"}>
                      This is a simple string to string match, a regex won't
                      work here.
                    </Typography>
                  </>
                ),
              },
              {
                name: "Membership",
                input: <FieldMembership />,
                description:
                  "Only show projects of which the user is a member of.",
              },
              {
                name: "Update period for running pipelines",
                input: <FieldUpdatePeriodRunning />,
                description:
                  "Update period (in seconds) for running pipelines.",
              },
            ]}
          />
        </DialogContent>
        <DialogActions>
          <Grid
            container
            direction={"row"}
            justifyContent={"space-evenly"}
            alignItems={"center"}
          >
            <Grid item>
              <Button onClick={onDialogImport}>Import</Button>
            </Grid>

            <Grid item>
              <Button onClick={onDialogExport}>Export</Button>
            </Grid>

            <Grid item>
              <Button onClick={onDialogSave}>Save</Button>
            </Grid>

            <Grid item>
              <Button onClick={onDialogCancel}>Cancel</Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ConfigController;
