/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { CssBaseline } from "@mui/material";

import { Provider as ThemeProvider } from "./providers/Theme";
import { Provider as ConfigProvider } from "./providers/Config";

import MainPage from "./MainPage";

const { version } = require("../package.json");

function App() {
  console.log(`Current version: ${version}`);

  return (
    <ThemeProvider>
      <CssBaseline />
      <ConfigProvider>
        <MainPage />
      </ConfigProvider>
    </ThemeProvider>
  );
}

export default App;
