/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

import { createTheme, ThemeOptions, ThemeProvider } from "@mui/material";

const DEFAULT_OPTIONS: ThemeOptions = {
  palette: {
    mode: "dark",
  },
};

const Provider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const theme = createTheme(DEFAULT_OPTIONS);

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export { Provider };
