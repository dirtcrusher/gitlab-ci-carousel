/**
 * This file is part of GitLab CI Carousel.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { createContext, useEffect, useState } from "react";

interface ConfigSchema {
  host: string;
  token: string;
  maxAge: number;
  swipePeriod: number;
  historyPerRef: number;
  hideDeletedRefs: boolean;
  nameFilter: string;
  membership: boolean;
  updatePeriodRunning: number;

  [key: string]: any;
}

type ConfigKey = keyof ConfigSchema;

interface ConfigContextSchema extends ConfigSchema {
  updateConfigEntry: (key: ConfigKey, value: string | number | boolean) => void;
  getCompleteConfig: () => ConfigSchema;
  setCompleteConfig: (config: ConfigSchema) => void;
  saveModifiedConfiguration: VoidFunction;
  cancelConfigurationModification: VoidFunction;
}

const DEFAULT_CONFIG_VALUES: ConfigSchema = {
  host: "https://www.gitlab.com/",
  token: "",
  maxAge: 24,
  swipePeriod: 30,
  historyPerRef: 3,
  hideDeletedRefs: true,
  nameFilter: "",
  membership: false,
  updatePeriodRunning: 2,
};

const loadInitialValues = (): ConfigSchema => {
  const storedConfig = localStorage.getItem("config");
  if (storedConfig) {
    return JSON.parse(storedConfig);
  } else {
    return DEFAULT_CONFIG_VALUES;
  }
};

const DEFAULT_CONTEXT_VALUES: ConfigContextSchema = {
  ...DEFAULT_CONFIG_VALUES,
  updateConfigEntry: () => {},
  getCompleteConfig: () => DEFAULT_CONFIG_VALUES,
  setCompleteConfig: () => {},
  saveModifiedConfiguration: () => {},
  cancelConfigurationModification: () => {},
};

const Context = createContext<ConfigContextSchema>(DEFAULT_CONTEXT_VALUES);

const Provider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [config, setConfig] = useState<ConfigSchema>(loadInitialValues());
  const [tmpConfig, setTmpConfig] = useState<ConfigSchema>(config);

  const updateConfigEntry = (
    key: ConfigKey,
    value: string | number | boolean
  ): void => {
    setTmpConfig({ ...tmpConfig, [key]: value });
  };

  useEffect(() => {
    localStorage.setItem("config", JSON.stringify(config));
  }, [config]);

  const saveModifiedConfiguration: VoidFunction = () => {
    setConfig(tmpConfig);
  };

  const cancelConfigurationModification: VoidFunction = () => {
    setTmpConfig(config);
  };

  const getCompleteConfig = (): ConfigSchema => {
    return config;
  };

  const setCompleteConfig = (newConfig: Record<string, any>) => {
    const tmp: ConfigSchema = DEFAULT_CONFIG_VALUES;

    for (const key in tmp) {
      if (key in newConfig) {
        tmp[key] = newConfig[key];
      }
    }

    setTmpConfig(tmp);
    setConfig(tmp);
  };

  return (
    <Context.Provider
      value={{
        ...config,
        updateConfigEntry,
        getCompleteConfig,
        setCompleteConfig,
        saveModifiedConfiguration,
        cancelConfigurationModification,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export { Context, Provider, ConfigContextSchema };
